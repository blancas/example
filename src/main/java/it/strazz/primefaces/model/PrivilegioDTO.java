package it.strazz.primefaces.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

public class PrivilegioDTO {
    public static final String PRIV_AUTO="PRIV_AUTO";
    public static final String PRIV_EDICION="PRIV_EDICION";
    public static final String PRIV_SEGURIDAD="PRIV_SEGURIDAD";
    private String clave;
    private String descripcion;
    public PrivilegioDTO(String privilegio, String descripcion) 
    {
        this.clave = privilegio;
        this.descripcion = descripcion;
    }
    public PrivilegioDTO() 
    {
    }
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public static String getPrivAuto() {
		return PRIV_AUTO;
	}
	public static String getPrivEdicion() {
		return PRIV_EDICION;
	}
	public static String getPrivSeguridad() {
		return PRIV_SEGURIDAD;
	}
    
}
