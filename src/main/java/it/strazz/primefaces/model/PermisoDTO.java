package it.strazz.primefaces.model;

import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

public class PermisoDTO {
    public static final String TIPO_NUEVO="N";
    public static final String TIPO_MODIFICACION="M";
    public static final String TIPO_ELIMINACION="E";
    private String clave;
    private String nombre;
    private List<RecursoSeguroDTO> permisos;

    private UsuarioDTO usuario;
    private Instant tsModificacion;
    private String tipo;


    public PermisoDTO(String clave, String nombre) {
        this.clave = clave;
        this.nombre = nombre;
    }

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<RecursoSeguroDTO> getPermisos() {
		return permisos;
	}

	public void setPermisos(List<RecursoSeguroDTO> permisos) {
		this.permisos = permisos;
	}

	public UsuarioDTO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}

	public Instant getTsModificacion() {
		return tsModificacion;
	}

	public void setTsModificacion(Instant tsModificacion) {
		this.tsModificacion = tsModificacion;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public static String getTipoNuevo() {
		return TIPO_NUEVO;
	}

	public static String getTipoModificacion() {
		return TIPO_MODIFICACION;
	}

	public static String getTipoEliminacion() {
		return TIPO_ELIMINACION;
	}

    
}
