package it.strazz.primefaces.model;

import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

public class UsuarioDTO implements Serializable {
    private String username;
    private String password;
    private Integer enabled;
    private Integer deleted;
    private String nombre;
    private String email;
    private Integer locked;
    private Instant ultimoCambioPassword;
    private Instant ultimoAcceso;
    private Integer numIntentosLogin;
    private Instant bloqueoIntentos;
    private Integer enrolado;
    private String ip;
    private Instant fechaRegistro;
    private String sessionId;
    private Integer cambiarPassword;
    private String usuarioRegistro;
    private List<RolDTO> roles;
    private List<PrivilegioDTO> privilegios;

    public UsuarioDTO(
    		String username,
		    String password, 
		    String nombre,
		    String email,
		    Integer locked,
		    Integer numIntentosLogin,  
		    String ip,
		    String sessionId,
		    Integer cambiarPassword )
    {
    	this.username =username;
    	this.password =  password;
    	this.nombre=  nombre;
    	this.email =  email;
    	this.locked =  locked;
    	this.numIntentosLogin =  numIntentosLogin;
    	this.ip =  ip;
    	this.sessionId =  sessionId;
    	this.cambiarPassword =  cambiarPassword;
    	
    }
    public UsuarioDTO() {
    }
    public UsuarioDTO(String username) {
        this.username = username;
    }

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	public Integer getDeleted() {
		return deleted;
	}

	public void setDeleted(Integer deleted) {
		this.deleted = deleted;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getLocked() {
		return locked;
	}

	public void setLocked(Integer locked) {
		this.locked = locked;
	}

	public Instant getUltimoCambioPassword() {
		return ultimoCambioPassword;
	}

	public void setUltimoCambioPassword(Instant ultimoCambioPassword) {
		this.ultimoCambioPassword = ultimoCambioPassword;
	}

	public Instant getUltimoAcceso() {
		return ultimoAcceso;
	}

	public void setUltimoAcceso(Instant ultimoAcceso) {
		this.ultimoAcceso = ultimoAcceso;
	}

	public Integer getNumIntentosLogin() {
		return numIntentosLogin;
	}

	public void setNumIntentosLogin(Integer numIntentosLogin) {
		this.numIntentosLogin = numIntentosLogin;
	}

	public Instant getBloqueoIntentos() {
		return bloqueoIntentos;
	}

	public void setBloqueoIntentos(Instant bloqueoIntentos) {
		this.bloqueoIntentos = bloqueoIntentos;
	}

	public Integer getEnrolado() {
		return enrolado;
	}

	public void setEnrolado(Integer enrolado) {
		this.enrolado = enrolado;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Instant getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Instant fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public Integer getCambiarPassword() {
		return cambiarPassword;
	}

	public void setCambiarPassword(Integer cambiarPassword) {
		this.cambiarPassword = cambiarPassword;
	}

	public String getUsuarioRegistro() {
		return usuarioRegistro;
	}

	public void setUsuarioRegistro(String usuarioRegistro) {
		this.usuarioRegistro = usuarioRegistro;
	}

	public List<RolDTO> getRoles() {
		return roles;
	}

	public void setRoles(List<RolDTO> roles) {
		this.roles = roles;
	}

	public List<PrivilegioDTO> getPrivilegios() {
		return privilegios;
	}

	public void setPrivilegios(List<PrivilegioDTO> privilegios) {
		this.privilegios = privilegios;
	}
    
}
