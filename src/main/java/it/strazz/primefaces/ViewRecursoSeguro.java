package it.strazz.primefaces.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

public class RecursoSeguroDTO {
    private String recurso;
    private String descripcion;
    private RecursoSeguroDTO recursoPadre;


    public RecursoSeguroDTO(String recurso, String descripcion, RecursoSeguroDTO recursoPadre) {
        this.recurso = recurso;
        this.descripcion = descripcion;
        this.recursoPadre = recursoPadre;
    }

    @Override
    public String toString() {
        return "RecursoSeguroDTO{" +
                "recurso='" + recurso + '\'' +
                '}';
    }

	public String getRecurso() {
		return recurso;
	}

	public void setRecurso(String recurso) {
		this.recurso = recurso;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public RecursoSeguroDTO getRecursoPadre() {
		return recursoPadre;
	}

	public void setRecursoPadre(RecursoSeguroDTO recursoPadre) {
		this.recursoPadre = recursoPadre;
	}
    
}
