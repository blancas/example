package it.strazz.primefaces;
import it.strazz.primefaces.columns.ColumnModel;
import it.strazz.primefaces.columns.ReflectionColumnModelBuilder;
import it.strazz.primefaces.model.User;
import it.strazz.primefaces.model.UsuarioDTO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.apache.commons.beanutils.MethodUtils;
@ManagedBean
@ViewScoped
public class ViewUsuarios implements Serializable{
	
	private List<UsuarioDTO> columns = new ArrayList<UsuarioDTO>(0);
	public void onGetList() 
	{
		columns.add(new UsuarioDTO(
	    		"AD0001",
			    "AD0001L", 
			    "Luis","lbln01@gmail.com",1,0,"0.0.0.0","Session 001",30));
	}
	
	public List<Object> getData(){
		try {
			return (List<Object>) MethodUtils.invokeExactStaticMethod(Class.forName(currentClass), "getAll", null);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public List<ColumnModel> getColumns() {
		return columns;
	}
	
	public String getCurrentClass() {
		return currentClass;
	}

	public void setCurrentClass(String currentClass) {
		this.currentClass = currentClass;
	}
}
